//
//  main.cpp
//  DataBase
//
//  Created by Alexander Chapchuk on 07/10/2017.
//  Copyright © 2017 Alexander Chapchuk. All rights reserved.
//

#include <iostream>

#include "CommandManager.hpp"
int main(int argc, const char * argv[]) {
    CommandManager command;
    command.addCommand("view","view all records");
    command.addCommand("delete","delete one record");
    command.addCommand("insert","insert one record");
    command.viewCommands();
    
    return 0;
}


