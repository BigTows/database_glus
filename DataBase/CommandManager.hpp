//
//  CommandManager.hpp
//  DataBase
//
//  Created by Alexander Chapchuk on 07/10/2017.
//  Copyright © 2017 Alexander Chapchuk. All rights reserved.
//

#ifndef CommandManager_hpp
#define CommandManager_hpp
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include "MessageHelper.hpp"
#endif /* CommandManager_hpp */

class CommandManager{
    
public:
    CommandManager();
    void addCommand(std::string,std::string);
    void viewCommands();
    const std::string HEADER = "All commands";
private:
        std::vector<std::pair<std::string, std::string>> commands;
};
