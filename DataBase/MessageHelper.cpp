//
//  MessageHelper.cpp
//  DataBase
//
//  Created by Alexander Chapchuk on 07/10/2017.
//  Copyright © 2017 Alexander Chapchuk. All rights reserved.
//

#include "MessageHelper.hpp"

void MessageHelper::log(std::string message){
    std::cout << message << "\n";
};
