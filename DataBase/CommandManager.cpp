//
//  CommandManager.cpp
//  DataBase
//
//  Created by Alexander Chapchuk on 07/10/2017.
//  Copyright © 2017 Alexander Chapchuk. All rights reserved.
//

#include "CommandManager.hpp"


CommandManager::CommandManager(){
    
}

void CommandManager::addCommand(std::string name, std::string description){
    commands.push_back(std::make_pair(name,description));
};

void CommandManager::viewCommands(){
    MessageHelper::log(HEADER);
    for (auto &command : commands)
       MessageHelper::log(command.first + " - " + command.second);
}




