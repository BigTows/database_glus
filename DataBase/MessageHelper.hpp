//
//  MessageHelper.hpp
//  DataBase
//
//  Created by Alexander Chapchuk on 07/10/2017.
//  Copyright © 2017 Alexander Chapchuk. All rights reserved.
//

#ifndef MessageHelper_hpp
#define MessageHelper_hpp

#include <stdio.h>
#include <string>
#include <iostream>
class MessageHelper{
public:
    static void log(std::string message);
};
#endif /* MessageHelper_hpp */
